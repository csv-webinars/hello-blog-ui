using System.Collections.Generic;
using System.Threading.Tasks;
using hello_blog_ui.Interfaces;
using hello_blog_ui.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace hello_blog_ui.Controllers
{
    public class AdministrationController : BaseController
    {
        public readonly IAdministrationApiManager _administrationApiManager;

        public AdministrationController(IAdministrationApiManager administrationApiManager)
        {
            _administrationApiManager = administrationApiManager;
        }

        public async Task<IActionResult> Index()
        {
            var cookieValue = GetCookie();
            var response = await _administrationApiManager.GetRolesAsync(cookieValue);
            var content = await response.Content.ReadAsStringAsync();
            var roles = JsonConvert.DeserializeObject<List<RoleModel>>(content);

            if (roles != null)
                return View(roles);
                
            return View();
        }
    }
}