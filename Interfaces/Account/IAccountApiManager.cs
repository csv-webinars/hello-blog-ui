using System.Net.Http;
using System.Threading.Tasks;
using hello_blog_ui.Entities;

namespace hello_blog_ui.Interfaces
{
    public interface IAccountApiManager
    {
        Task<HttpResponseMessage> Register(AccountCredentials accountCredentials);
        Task<LoginApiResponse> Login(AccountCredentials accountCredentials);
        Task<HttpResponseMessage> Logout();
    }
}