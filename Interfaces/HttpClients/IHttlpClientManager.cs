using System;
using System.Net.Http;
using System.Threading.Tasks;
using hello_blog_ui.Entities;

namespace hello_blog_ui.Interfaces
{
    public interface IHttlpClientManager
    {
        Task<HttpResponseMessage> GetAsync(string url, string cookieValue = null);
        Task<HttpResponseMessage> PostAsync(string url, HttpContent content, string cookieValue);
        Task<LoginApiResponse> PostWithHandlerAsync(Uri uri, HttpContent content);
        Task<HttpResponseMessage> PutAsync(string url, HttpContent content);
        Task<HttpResponseMessage> PutAsync(string url, HttpContent content, string cookieValue);
        Task<HttpResponseMessage> DeleteAsync(string url, string cookieValue);
    }
}