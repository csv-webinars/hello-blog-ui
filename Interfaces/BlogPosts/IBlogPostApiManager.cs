using System.Net.Http;
using System.Threading.Tasks;
using hello_blog_ui.Models;

namespace hello_blog_ui.Interfaces
{
    public interface IBlogPostApiManager
    {
        Task<HttpResponseMessage> GetAllAsync();
        Task<HttpResponseMessage> GetBlogPostByIdAsync(int blogPostId);
        Task<HttpResponseMessage> CreateAsync(BlogPostApiModel blogPost, string cookieValue);
        Task<HttpResponseMessage> UpdateAsync(BlogPostApiModel blogPost, string cookieValue);
        Task<HttpResponseMessage> DeleteAsync(int blogPostId, string cookieValue);
    }
}