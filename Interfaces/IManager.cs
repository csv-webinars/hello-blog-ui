using System.Collections.Generic;
using System.Threading.Tasks;

namespace hello_blog_ui.Interfaces
{
    ///<summary> 
    /// Added to show ISP - an abstraction should have only what is needed (not a fat one)
    /// We decided to segregate into multiple interfaces
    ///</summary>
    public interface IManager
    {
        Task UpdateNumberOfViews();
        Task<List<string>> BuildImageList();
    }
}