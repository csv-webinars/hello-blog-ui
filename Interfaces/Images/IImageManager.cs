using System.Collections.Generic;
using System.Threading.Tasks;

namespace hello_blog_ui.Interfaces
{
    public interface IImageManager
    {
        Task<List<string>> BuildImageList();
    }
}