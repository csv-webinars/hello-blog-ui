using System.Net.Http;
using System.Threading.Tasks;

namespace hello_blog_ui.Interfaces
{
    public interface IAdministrationApiManager
    {
        Task<HttpResponseMessage> GetRolesAsync(string cookieValue);
    }
}