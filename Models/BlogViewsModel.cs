using System;

namespace hello_blog_ui.Models
{
    public class BlogViewsModel
    {
        public int Id { get; set; }
        public int Views { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
