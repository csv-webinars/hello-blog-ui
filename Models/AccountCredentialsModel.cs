
using System.ComponentModel.DataAnnotations;

namespace hello_blog_ui.Models
{
    public class AccountCredentialsModel
    {
        [Required, EmailAddress]
        public string Email { get; set; }

        [Required, DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
