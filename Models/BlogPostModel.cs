using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace hello_blog_ui.Models
{
    public class BlogPostModel
    {
        public List<BlogPostApiModel> BlogPosts { get; set; }
        public string ExecutionTime { get; set; }
        public string BlogNumberOfViews { get; set; }
        public bool IsAuthenticated { get; set; }

        [BindProperty]
        public IFormFile BulkBlogPosts { get; set; }
    }
}
