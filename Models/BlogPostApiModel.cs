
using System;
using System.ComponentModel.DataAnnotations;

namespace hello_blog_ui.Models
{
    public class BlogPostApiModel
    {
        public int Id { get; set; }
        [Required, MaxLength(80)]
        public string Title { get; set; }

        [Required]
        public string Content { get; set; }

        [Required, MaxLength(50)]
        public string Label { get; set; }
        public string NumberOfViews { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string ImageUrl { get; set; }
        public bool Edit { get; set; }
    }
}
