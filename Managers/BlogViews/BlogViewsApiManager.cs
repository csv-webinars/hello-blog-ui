using System.Net.Http;
using System.Threading.Tasks;
using hello_blog_ui.Interfaces;

namespace hello_blog_ui
{
    public class BlogViewsApiManager : IBlogViewsApiManager
    {
        private readonly IHttlpClientManager _httlpClientManager;

        public BlogViewsApiManager(IHttlpClientManager httlpClientManager)
        {
            _httlpClientManager = httlpClientManager;
        }

        public Task<HttpResponseMessage> GetAsync()
        {
            return _httlpClientManager.GetAsync(Constants.Api.BlogViews);
        }

        public Task<HttpResponseMessage> UpdateAsync()
        {
            return _httlpClientManager.PutAsync(Constants.Api.BlogViews, null);
        }
    }
}