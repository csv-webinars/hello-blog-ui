using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using hello_blog_ui.Entities;
using hello_blog_ui.Interfaces;
using Newtonsoft.Json;

namespace hello_blog_ui.Managers
{
    public class AccountApiManager : IAccountApiManager
    {
        private readonly IHttlpClientManager _httlpClientManager;

        public AccountApiManager(IHttlpClientManager httlpClientManager)
        {
            _httlpClientManager = httlpClientManager;
        }

        public async Task<HttpResponseMessage> Register(AccountCredentials accountCredentials)
        {
            var payload = JsonConvert.SerializeObject(accountCredentials);
            HttpContent content = new StringContent(payload, Encoding.UTF8, "application/json");
            return await _httlpClientManager.PostAsync(Constants.Api.Register, content, null);
        }

        public async Task<LoginApiResponse> Login(AccountCredentials accountCredentials)
        {
            var payload = JsonConvert.SerializeObject(accountCredentials);
            HttpContent content = new StringContent(payload, Encoding.UTF8, "application/json");
            return await _httlpClientManager.PostWithHandlerAsync(new Uri(Constants.Api.Login), content);
        }

        public async Task<HttpResponseMessage> Logout()
        {
            return await _httlpClientManager.PostAsync(Constants.Api.Logout, null, null);
        }
    }
}