using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using hello_blog_ui.Interfaces;
using hello_blog_ui.Models;
using Newtonsoft.Json;

namespace hello_blog_ui.Managers
{
    ///<summary> 
    /// Responsibility : build the blogPosts that will be showed in the UI
    /// Class obtained applying 
    ///     - SRP (only one responsibility, pass other responsibilities to  IImageManager and IBlogPostApiManager)
    ///     - DIP (We inject IHttpClientManager manager to do the actual Http calls)
    ///         - Regarding memory issue we should not instantiate classes and let .NET to do that.
    ///             - Just register the service in the Startup class -> RegisterServices method
    ///</summary>
    public class BlogPostManager : IBlogPostManager
    {
        private readonly IImageManager _imageManager;
        private readonly IBlogPostApiManager _blogPostApiManager;
        private readonly IBlogViewsApiManager _blogViewsApiManager;

        public BlogPostManager(IBlogPostApiManager blogPostApiManager, 
                               IImageManager imageManger,
                               IBlogViewsApiManager blogViewsApiManager)
        {
            _imageManager = imageManger;
            _blogPostApiManager = blogPostApiManager;
            _blogViewsApiManager = blogViewsApiManager;
        }

        public async Task<List<BlogPostApiModel>> GetBlogPosts()
        {
            List<BlogPostApiModel> blogPosts = new List<BlogPostApiModel>();

            var response = Task.Run(() => _blogPostApiManager.GetAllAsync());
            var images = Task.Run(() => _imageManager.BuildImageList());
            
            // Task.WaitAll(response, images);
            await Task.WhenAll(response, images);

            if (response != null && response?.GetAwaiter().GetResult() != null)
            {
                string apiResponse = response.Result.Content.ReadAsStringAsync().Result;
                blogPosts = JsonConvert.DeserializeObject<List<BlogPostApiModel>>(apiResponse);

                var random = new Random();

                foreach (var blogPost in blogPosts)
                {
                    var rnd = random.Next(images.Result.Count);
                    blogPost.ImageUrl = images.Result[rnd];
                }
            }

            return blogPosts;
        }

        public async Task<BlogPostApiModel> GetBlogPostById(int blogPostId)
        {
            var blogPost = new BlogPostApiModel();
            var response = await _blogPostApiManager.GetBlogPostByIdAsync(blogPostId);

            if (response != null)
            {
                var apiResponse = response.Content.ReadAsStringAsync().Result;
                blogPost = JsonConvert.DeserializeObject<BlogPostApiModel>(apiResponse);
            }

            return blogPost;
        }

        public async Task<bool> Update(BlogPostApiModel blogPost, string cookieValue)
        {
            var response = await _blogPostApiManager.UpdateAsync(blogPost, cookieValue);
            if (response?.StatusCode != HttpStatusCode.OK)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> DeleteBlogPostById(int blogPostId, string cookieValue)
        {
            var response = await _blogPostApiManager.DeleteAsync(blogPostId, cookieValue);
            if (response?.StatusCode != HttpStatusCode.OK)
            {
                return false;
            }

            return true;
        }
    }
}