using System.Net.Http;
using System.Threading.Tasks;
using hello_blog_ui.Interfaces;

namespace hello_blog_ui
{
    public class BlogPostViewsApiManager : IBlogPostViewsApiManager
    {
        private readonly IHttlpClientManager _httlpClientManager;

        public BlogPostViewsApiManager(IHttlpClientManager httlpClientManager)
        {
            _httlpClientManager = httlpClientManager;
        }

        public Task<HttpResponseMessage> GetAsync(int blogPostId)
        {
            return _httlpClientManager.GetAsync(Constants.Api.BlogPostViews + "/" + blogPostId);
        }

        public Task<HttpResponseMessage> UpdateAsync(int blogPostId)
        {
            return _httlpClientManager.PutAsync(Constants.Api.BlogPostViews + "/" + blogPostId, null);
        }
    }
}