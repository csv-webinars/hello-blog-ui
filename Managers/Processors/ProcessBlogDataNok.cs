using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace hello_blog_ui.Managers
{
    ///<summary>
    /// Class obtained writting initial code tha breaks the OCP (Close for modification) 
    ///</summary>
    public class ProcessBlogDataNok
    {
        public async Task<bool> PublishFromFile(IFormFile formFile)
        {
            if (formFile.FileName.EndsWith(".xml"))
            {
                // parse XML file to DTO object
                // publish tp xml service
            }

            // breaks Close
            if (formFile.FileName.EndsWith(".json"))
            {
                // parse json file to DTO object
                // publish to json service
            }

            return await Task.FromResult(true);
        }
    }
}