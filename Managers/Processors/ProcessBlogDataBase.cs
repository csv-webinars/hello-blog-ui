namespace hello_blog_ui.Managers
{
    ///<summary>
    /// Class obtained applying 
    ///     - OCP (Generate a generic report()
    ///         - If child class need a different functionality they can ovveride
    ///</summary>
    public abstract class ProcessBlogDataBase
    {
        public virtual string GetReport()
        {
            return "Generic report";
        }
    }
}